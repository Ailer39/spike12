#pragma once
#include <map>
#include <string>
#include "IMessaging.h"
#include "MessageTypes.h"
#include "MessageBase.h"

using namespace std;

class MessagingSystem {
private:
	MessagingSystem() {};
	map<string, Messaging*> _registeredObjects;
public:
	static MessagingSystem* GetInstance();
	void RegisterObject(string name, Messaging* obj);
	void SendMessage(string targetObj, MessageBase* message);
	void BroadcastMessage(MessageBase* message);
};