#include "ObjectRemovedMessage.h"

ObjectRemovedMessage::ObjectRemovedMessage(GameObject* o)
{
	SetType(MessageTypes::ObjectRemoved);
	_gameObj = o;
}

GameObject* ObjectRemovedMessage::GetGameObject()
{
	return _gameObj;
}