#pragma once

#include <iostream>
#include "IMessaging.h"
#include "ObjectRemovedMessage.h"

using namespace std;

class Score:
	public Messaging
{
private:
	int _value;
	Score() {};
public:
	static Score* GetInstance();
	int GetScore();
	void AddPoints(int value);
	void PrintScore();
	void HandleMessage(MessageBase* message);
	void ExecuteMessage();
};