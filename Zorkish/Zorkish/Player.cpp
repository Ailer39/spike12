#include "Player.h"

Player::Player(Location *startLocation)
{
	_currentLocation = startLocation;
}

Location* Player::GetCurrentPosition()
{
	return _currentLocation;
}

void Player::MovePlayer(Direction direction)
{
	if (_currentLocation != nullptr)
	{
		Location* targetLocation = _currentLocation->GetLocation(direction);

		if (targetLocation != nullptr)
		{
			_currentLocation = targetLocation;
		}

		cout << "Current location: " << _currentLocation->GetName() << endl;
	}
}

void Player::ExecuteMessage()
{
	if (!messages.empty())
	{
		MessageBase* message = messages.top();
		messages.pop();

		if (message->GetType() == MessageTypes::Attack)
		{
			GameObject* gameObj = ((AttackMessage*)message)->GetGameObject();
			HealthComponent* health = ((HealthComponent*)gameObj->GetComponent(HealthComponent::type));

			if (health != nullptr)
			{
				health->ChangeHealth(-50);
				health->PrintHealth();

				if (health->GetHealth() <= 0)
				{
					cout << "You killed " << gameObj->GetName() << endl;
					MessagingSystem::GetInstance()->BroadcastMessage(new ObjectRemovedMessage(gameObj));
				}
			}
			else
			{
				cout << "You can not attack " << gameObj->GetName() << endl;
			}
		}
	}
}

void Player::HandleMessage(MessageBase* message)
{
	if (message->GetType() == MessageTypes::Attack)
	{
		messages.push(message);
		ExecuteMessage();
	}
}