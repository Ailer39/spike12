#include "CommandHelper.h"


string CommandHelper::GetGameObjectNameFromCommand(string command, string key)
{
	int index = command.find(key) + key.length();
	string objName = command.substr(index, command.size());
	objName = objName.erase(0, objName.find_first_not_of(" "));
	objName = objName.erase(objName.find_last_not_of(" ") + 1);
	return objName;
}