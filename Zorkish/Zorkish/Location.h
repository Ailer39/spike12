#pragma once

#include <string>
#include <vector>
#include <iostream>
#include "GameObject.h"
#include "Direction.h"
#include "ObjectRemovedMessage.h"
#include "MessagingSystem.h"
#include <regex>

using namespace std;

class Location :
	public GameObject
{
private:
	Location* _connectedNotes[4];
	int _id;
public:
	Location(int id, string name, string description);
	void AddLocation(Location* newLocation, int direction);
	Location* GetLocation(Direction direction);
	int GetId();
	void PrintConnections();
	void Look();
	void HandleMessage(MessageBase* message);
	void ExecuteMessage();
};