#pragma once
#include "Location.h"
#include "Direction.h"
#include "Inventory.h"
#include "IMessaging.h"
#include "HealthComponent.h"
#include <regex>
#include "AttackMessage.h"
#include "MessageTypes.h"
#include "MessagingSystem.h"
#include "ObjectRemovedMessage.h"

class Player :
	public Messaging
{
private:
	Location* _currentLocation;

public:
	void MovePlayer(Direction direction);
	Location* GetCurrentPosition();
	Player(Location* startLocation);
	void HandleMessage(MessageBase* message);
	void ExecuteMessage();
};

