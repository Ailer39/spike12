#pragma once
#include "MessageTypes.h"

class MessageBase 
{
private:
	enum MessageTypes _type;
protected: 
	void SetType(enum MessageTypes value) { _type = value; }
public: 
	enum MessageTypes GetType() { return _type; }
};