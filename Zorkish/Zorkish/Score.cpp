#include "Score.h"

static Score* instance = nullptr;

Score* Score::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Score();
	}

	return instance;
}

int Score::GetScore()
{
	return _value;
}

void Score::AddPoints(int value)
{
	_value += value;
}

void Score::PrintScore()
{
	cout << "You're score is: " << GetScore() << endl;
}

void Score::HandleMessage(MessageBase* message)
{
	if (message->GetType() == MessageTypes::ObjectRemoved)
	{
		messages.push(message);
	}
}

void Score::ExecuteMessage()
{
	if (!messages.empty())
	{
		ObjectRemovedMessage* message = (ObjectRemovedMessage*)messages.top();
		AddPoints(20);
		PrintScore();
		messages.pop();
	}
}