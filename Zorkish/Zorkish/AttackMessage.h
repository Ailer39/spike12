#pragma once
#include "MessageBase.h"
#include "GameObject.h"
#include "Location.h"

class AttackMessage :
	public MessageBase
{
private: 
	GameObject* _gameObj;
	Location* _gameObjLocation;
public:
	GameObject* GetGameObject(){ return _gameObj; }
	Location* GetGameObjectLocation(){ return _gameObjLocation; }

	AttackMessage(GameObject* gameObj, Location* gameObjLocation)
	{
		SetType(MessageTypes::Attack);
		_gameObj = gameObj;
		_gameObjLocation = gameObjLocation;
	};
};