#include "MainState.h"

static MainState* instance;

MainState* MainState::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new MainState();
	}

	return instance;
}

void MainState::Show(StateManager* context)
{
	cout << "Main Menu" << endl;
	cout << "Welcome to Zorkish Adventure and Play" << endl;
	cout << "1. Select Adventure and Play" << endl;
	cout << "2. Hall of Fame" << endl;
	cout << "3. Help" << endl;
	cout << "4. About" << endl;
	cout << "5. Quit" << endl;
	cout << endl;
	cout << "Select 1-5:> _" << endl;

	int nextState;
	cin >> nextState;
	context->ChangeState(nextState);
}