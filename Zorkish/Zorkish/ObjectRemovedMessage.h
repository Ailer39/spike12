#pragma once
#include "GameObject.h"
#include "MessageBase.h"

class GameObject;
class ObjectRemovedMessage :
	public MessageBase
{
private:
	GameObject* _gameObj;
public:
	GameObject* GetGameObject();
	ObjectRemovedMessage(GameObject* o);
};