#include "GameLogic.h"

static GameLogic* instance;

GameLogic* GameLogic::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new GameLogic();
	}

	return instance;
}

void GameLogic::InitLevel(Location* level)
{
	_player = new Player(level);
	_commandManager = new CommandManager(_player);
	RegisterObjects();
}

void GameLogic::RegisterObjects()
{
	MessagingSystem* messageSystem = MessagingSystem::GetInstance();
	messageSystem->RegisterObject("player", _player);
	messageSystem->RegisterObject("score", Score::GetInstance());
}

void GameLogic::Show(StateManager* context)
{
	if (_player != nullptr)
	{
		Location* currentLocation = _player->GetCurrentPosition();
		cout << currentLocation->GetDescription() << endl;
		string command;
		cin.ignore(1);
		cin.clear();

		while (!_gameFinished)
		{
			cout << "Input: ";
			getline(cin, command);
			_commandManager->ExecuteCommand(command);

			_player->GetCurrentPosition()->ExecuteMessage();
			Score::GetInstance()->ExecuteMessage();
		}
	}
}