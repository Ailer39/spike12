#pragma once
#include <stack>
#include "MessageTypes.h"
#include "MessageBase.h"

using namespace std;

class Messaging {
protected: 
	stack<MessageBase*> messages;
public: 
	virtual void HandleMessage(MessageBase* message) {};
	virtual void ExecuteMessage() {};
};