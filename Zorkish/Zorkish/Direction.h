#pragma once

enum Direction
{
	North = 0,
	South = 1,
	East = 2,
	West = 3
};

typedef enum Direction Direction;