#pragma once
#include "Equipment.h"
#include "Inventory.h"
#include <iostream>

using namespace std;

class Backpack :
	public Equipment
{
private:
	Inventory* _inventory;
public:
	Backpack();
	void Print();
	void Add(Equipment* item);
	Equipment* UseItem(string itemName);
	void RemoveItem(string itemName);
	void LookAt();
	void LookIn();
};