#include "StateBase.h"
#include "StateManager.h"

class StateManager;
class MainState :
	public StateBase
{
private:
	MainState() {};

public:
	static MainState* GetInstance();
	void Show(StateManager* context);
};