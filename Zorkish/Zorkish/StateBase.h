#pragma once

#include "StateManager.h"
#include <string>
#include <iostream>
#include<conio.h>

using namespace std;

class StateManager;

class StateBase
{
public:
	virtual void Show(StateManager* context);
};